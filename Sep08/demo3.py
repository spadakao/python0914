# encoding=utf-8
# 沒有 encoding 的設定 不能打中文
print ''
print 'abc'
print 'qwer','jkll;'
print '中文'
print type('中文')
print u'中文', type(u'中文')

# 編碼不同的問題
# utf-8 的情況下 字的大小會跟 big 5 不同，big 5 的話 len('中文') = 4
print len('中文'), len(u'中文')

# int 做運算時 py2 不會自動轉 floaㄗ
print 4/2
print 5/2