# encoding=utf-8
list1 = list('abcde')
print type(list1), list1, hex(id(list1))
list2 = list1
print type(list2), list2, hex(id(list2))

list3 = list1[:]
list4 = list(list1)

list1[0] = '*'
list2[1] = 'o'
list1[2] = 'x'
list2[3] = 'm'

# list3 & list4 重新建構一個 list 所以不受影響
print list1, list2, list3, list4
