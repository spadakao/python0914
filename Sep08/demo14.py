dayOfWeek = ('Sunday', 'Monday')
# tuple can not add a string, this will error
# dayOfWeek += 'Tuesday'

# tuple can add a tuple(with a comma)
dayOfWeek += 'Tuesday',
print dayOfWeek
print dayOfWeek[0], dayOfWeek[2]
print [len(day) for day in dayOfWeek]

# tuple is different from string
print 'Wednesday' * 5
print ('Wednesday',) * 5

print '----------------------------------'


# more tuple
def splitHead(seq):
    return seq[0], seq[1:]


dayOfWeek2 = ['Sunday', 'Monday', 'Tuesday',
              'Wednesday', 'Thursday', 'Friday', 'Saturday']

first, remain = splitHead(dayOfWeek2)
print first, remain
