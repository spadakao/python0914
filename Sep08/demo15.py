# set will delete duplicate
set1 = {'P', 'I', 'N', 'E', 'A', 'P', 'P', 'L', 'E'}
set2 = {'B', 'A', 'N', 'A', 'N', 'A'}

print set1, set2

set1.add('p')
set1.add('p')

print set1

print '----------------------------------------'
# union of sets
set3 = set1 | set2
print set3

# and of sets
set4 = set1 & set2
print set4

# xor of sets
set5 = set1 ^ set2
print set5

# or = and + xor
set6 = set4 | set5
print set6
print set3 == set6

# include or not?
set7 = {'A', 'N', 'L'}
print set7 < set2, set7 < set1
print set1 - set7, set2 - set7

# length of set
print len(set1), len(set2), len(set3)
