# encoding=utf-8
class Person:
    def __init__(self, age):
        self.age = age


# py 不可變的特性，改值後記憶體位置也會跟著改
age = 38
print 'when age = 38, age id = %s' % hex(id(age))
age = 39
print 'after age = 39, age id = %s' % hex(id(age))

# py object 可變
person1 = Person(38)
print 'before:: person id = %s, person.age id = %s' % (hex(id(person1)), hex(id(person1.age)))

person1.age = 39
print 'after::: person id = %s, person.age id = %s' % (hex(id(person1)), hex(id(person1.age)))
