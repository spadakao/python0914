x = 5
y = 200
print x, y

temp = x
x = y
y = temp
print x, y

# using Tuple to swap
a = 2
b = "500 xxx"
print a, b
a, b = b, a
print a, b
