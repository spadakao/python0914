r = 150
pi = 3.1415926

print 'radius = %f, area = %f' % (r, r * r * pi)
#
print 'radius = %.2f, area = %.2f' % (r, r * r * pi)
# key & value
print 'radius = %(radius).2f, area = %(area).2f' % {'radius': r, 'area': r * r * pi}
#
print 'radius = {}, area = {}'.format(r, r * r * pi)
#
print 'radius = {:.2f}, area = {:.2f}'.format(r, r * r * pi)
print 'radius = {0:.2f}, area = {1:.2f}'.format(r, r * r * pi)
print 'radius = {1:.2f}, area = {0:.2f}'.format(r * r * pi, r)
print 'radius = {radius:.2f}, area = {area:.2f}'.format(radius=r, area=r * r * pi)

print '------------------------------'
