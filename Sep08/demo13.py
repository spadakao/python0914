# encoding=utf-8

dayOfWeek = ['Sunday', 'Monday', 'Tuesday',
             'Wednesday', 'Thursday', 'Friday', 'Saturday']

lengthArray = []
# 笨方法（？
for day in dayOfWeek:
    lengthArray.append(len(day))

print lengthArray

# py 很聰明～XD
print [len(day) for day in dayOfWeek]
print [day + ' has length = ' + str(len(day)) for day in dayOfWeek]

# tuple
sun, mon, tue, wed, thu, fri, sat = dayOfWeek
print sun, fri, sat
print 'my data = ', tue, wed, thu

# list
numList = [3, 1, 4, 5, 12, 30, 56, 54, 98, 200]
over30AndSorted = sorted(i for i in numList if i > 30)
print over30AndSorted

under40 = sorted((i for i in numList if i < 40), reverse=True)
print under40

# range will return a list
print '---------------------------------------------------'
startRange = range(0, 20)
print startRange, type(startRange)

startRange2 = range(0, 20, 2)
print startRange2

startRange3 = range(0, 20, 3)
print startRange3

print '---------------------------------------------------'
# list + list will be a longer list, not add the elements in both of list
startRange4 = range(0, 10)
print startRange4 + startRange4

# want to add(or other operate) the elements in both of list, we need the "numpy" package
import numpy as np

nRange1 = np.array(range(0, 20))
print nRange1, type(nRange1)
print nRange1 + nRange1
print nRange1 * nRange1

print np.arange(0, 9.5, 0.25) # arange(start, end, step)