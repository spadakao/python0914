str1 = 'abcde12345' * 3
list1 = list(str1)
print list1
print '------------------------------'
list2 = list(str1)
list2[:15] = '~'
print list2
print '------------------------------'
list3 = list(str1)
del list3[0:15]

list4 = list(str1)
list4[0:15] = []

print list3, list4
print '------------------------------'
list5 = list(str1)
# need to give enough 'x' to insert
list5[::2] = 'x' * len(list5[::2])
print list5
print '------------------------------'
list6 = list(str1)
del list6[::3]
print list6
print '------------------------------'
list7 = list('abcdefg')
list7.append('xyz')
print list7
print '------------------------------'
list8 = list('abcdefg')
list8.extend('xyz')
print list8
print '------------------------------'
list9 = list('abcdefg')
list9 += 'xyz'
print list9
print '------------------------------'
list10 = list(str1)
print list10.count('4')
print list10
print list10.pop(), list10
for i in range(1, len(list10)-1):
    print list10.pop(), list10
print '------------------------------'
list11 = list(str1)
list11.reverse()
print list11
print '------------------------------'
list12 = list(str1)
list12.sort()
print list12
