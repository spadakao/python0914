# encoding=utf-8

# one star "*" 可以吃很多參數
def sample_variable_arguments(fix1, fix2, *args):
    print "fix part ==> ", fix1, fix2
    for arg in args:
        print 'variable arg: ', arg


sample_variable_arguments("fix variable", 500, True, False, 3.14, "hello world")

l1 = [True, False, 3.14, "hello world"]
sample_variable_arguments("fix variable", 500, l1)

t1 = (True, False, 3.14, "hello world")
t1 += ('welcome',)
sample_variable_arguments("fix variable", 500, t1)

s1 = {True, False, 3.14, "hello world"}
sample_variable_arguments("fix variable", 500, s1)

# 可用來拆解各個 element
sample_variable_arguments("fix variable", 500, *l1)
sample_variable_arguments("fix variable", 500, *t1)
sample_variable_arguments("fix variable", 500, *s1)

d1 = {'name': 'Mark', 'age': 42}
sample_variable_arguments("fix variable", 500, d1)
sample_variable_arguments("fix variable", 500, *d1)
