# if we want to use function from other module, we need to import first
from Sep14.demoModule import foo
import Sep14.demoModule as d

print foo(3, 4)
print d.foo(3, 4), d.bar(5, 6)
