# encoding=utf-8
sales = {'iphone7': 1000, 'iphone7plus': 1500,
         'iphone8': 3000, 'iphone8plus': 4000}
print sales['iphone7plus'], sales['iphone8plus']

# this line will KeyError: 'iphoneX'
# print sales['iphoneX']

# check if this key exist
print sales.get('iphone7'), sales.get('iphoneX')
print 'iphone7' in sales, 'iphone' in sales

# get all keys
print [key for key in sales.keys()]
print [key + '/' + str(sales[key]) for key in sales.keys()]

# print value
total = 0
for value in sales.values():
    total += value
    print value
print total

# item 是一組 key value 的組合，[0] 是 key，[1] 是 value
for item in sales.items():
    print type(item), len(item)
    print item[0], item[1]

sales['iphoneX'] = 2500
print  [key + ':' + str(value) for key, value in sales.items()]

# update
sales.update({'iphone7': 500, 'iphone7plus': 1300, 'ipad': 2000})
total = 0
for value in sales.values():
    total += value
    print value
print 'new total = ', total
