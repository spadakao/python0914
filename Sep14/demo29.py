# encoding=utf-8
import requests

# requests.patch() 應用，update 資料
url = 'https://pylesson-23f40.firebaseio.com/'
url4 = url + 'data4_array.json'

# 原 array l1 = ['hello', '中文', 100, False, 3.14159]
res4 = requests.patch(url4, json={'0': '我在這', '5': 2.968})
print res4.status_code, res4.content

url5 = url + 'data5_dict.json'
res5 = requests.patch(url5, json={'course': 'BDR', 'material': 'R + visual studio'})
print res5.status_code, res5.content
