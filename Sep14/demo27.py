# encoding=utf-8
from urllib2 import urlopen
from PIL import Image

url = 'http://www.cwb.gov.tw/V7/observe/satellite/Data/sao/sao.jpg'
fileToSave = urlopen(url)
image = Image.open(fileToSave)
image.save('images/orig.jpg')

# image resize
half = (image.size[0] / 2, image.size[1] / 2)
# 消除鋸齒
half_image = image.resize(half, Image.ANTIALIAS)
half_image.save('images/half.jpg')

# image rotate
rotate = image.transpose(Image.ROTATE_90)
rotate.save('images/r90.jpg')
r180 = image.transpose(Image.ROTATE_180)
r270 = image.transpose(Image.ROTATE_270)
r180.save('images/r180.jpg')
r270.save('images/r270.jpg')
