def sample_key_arguments(fix1, fix2, **kargs):
    print 'fix args: ', fix1, fix2
    for key in kargs:
        print 'key/value pair = %s, %s' % (key, kargs[key])


sample_key_arguments('hihi', 3.14, name='Mark', age=43, location='TPE')

obj = {'name': 'Mark', 'age': 43, 'location': 'TPE'}
sample_key_arguments('hihi', 3.14, **obj)
