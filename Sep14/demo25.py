# encoding=utf-8
import requests

url = 'https://bugzilla.mozilla.org/rest/bug/35'

result = requests.get(url)
print result.status_code, result.headers['content-type']
print '-------------'
for bug in result.json()['bugs']:
    print bug['alias'], bug['assigned_to']
    for each in bug['cc']:
        print 'cc to: ', each
    for eachDetail in bug['creator_detail']:
        print eachDetail, bug['creator_detail'][eachDetail]