# encoding=utf8
import Tkinter
import tkFont


def motion(ev):
    message.config(text='move to [%d, %d]' % (ev.x, ev.y))


# set a new window
top = Tkinter.Tk()
myFont = tkFont.Font(family='Source Code Pro', size=30)
Title = 'Write something'

message = Tkinter.Message(top, text=Title)
message.config(bg='blue', font=myFont, fg='white')
message.bind('<Motion>', motion)

# set all items
message.pack()

# open new window
top.mainloop()
