# encoding=utf8
import Tkinter
import tkFont

x = 0


def btnClick():
    global x
    label.config(text='# %d is clicked' % x)
    x += 1
    # print 'Hello!'


# set a new window
top = Tkinter.Tk()
myFont = tkFont.Font(family='Arial', size=30)
label = Tkinter.Label(top, text='Hello Tk for python2', padx=30, pady=10,
                      fg='#FF0', bg='#000', font=myFont)

# mac 系統下的 button 是 mac 內建的，Tkinter 無法改變
btn1 = Tkinter.Button(top, text='Push!', command=btnClick, bg='#0FF', font=myFont)

# set items
label.pack()
btn1.pack()

# open new window
top.mainloop()
