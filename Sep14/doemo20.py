def sample_var_args_call(arg1, arg2, arg3):
    print '---'
    print 'arg1: ', arg1
    print 'arg2: ', arg2
    print 'arg3: ', arg3

# tuple
args = ('two', 3)
sample_var_args_call(1, *args)

# list
args2 = ['x', 'y', 'z']
sample_var_args_call(*args2)

# set
args3 = {'p', 'q', 'R'}
sample_var_args_call(*args3)

# dictionary
args4 = {'X': 'xmen', 'Y': 'Y-combinator', 'Z': 'Zoo'}
sample_var_args_call(*args4)
