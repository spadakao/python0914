# encoding=utf-8
import requests

# requests.post() 應用，新增資料
url = 'https://pylesson-23f40.firebaseio.com/'

url1 = url + 'bid1.json'
record = {'name': 'Mark', 'quantity': 1}
res1 = requests.post(url1, json=record)
print res1.status_code, res1.content

for i in range(1, 10):
    rec = {'name': 'Mark', 'quantity': i}
    res2 = requests.post(url1, json=rec)
    print res2.status_code, res2.content
