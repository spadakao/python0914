# encoding=utf-8
# practice file IO
# read file

# 這個寫法要記得 close()
# f1 = open('data/Python_Introduction')
# readme = f1.read()  # 2602
# f1.close()

# 這個寫法 出 with 的範圍後會自動 close()，所以不用自己 close()
with open('data/Python_Introduction') as f1:
    readme = f1.read()  # 2602
print type(readme), len(readme)
print readme

# write file
f2 = open('data/clone', 'w')
f2.write(readme)
f2.close()

readTest = readme.decode('UTF-8')
print type(readTest), len(readTest)
print readTest

f3 = open('data/clone', 'w')
f3.write(readTest.encode('UTF-8'))
f3.close()
