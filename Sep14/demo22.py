from datetime import datetime

now = datetime.now()

print "repr: " + repr(now)
print  "str: " + str(now)

# in container => repr format
print now
print [now]
print (now,)
print {'k1': now}

# if you want to stay str format in a container, use str()
print [str(now)]
