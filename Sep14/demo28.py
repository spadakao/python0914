# encoding=utf-8
import requests

# requests.put() 應用，放資料上去，只會有一筆
url = 'https://pylesson-23f40.firebaseio.com/'

url1 = url + 'data1_string.json'
res1 = requests.put(url1, json="Hello firebase from python 0914")
print res1.status_code, res1.content

url2 = url + 'data2_chinese.json'
res2 = requests.put(url2, json='中文輸入')
print res2.status_code, res2.content

url3 = url + 'data3_utf.json'
res3 = requests.put(url3, json=u'中文輸入 utf-8')
print res3.status_code, res3.content

l1 = ['test', '中文', 100, False, 3.14159]
url4 = url + 'data4_array.json'
res4 = requests.put(url4, json=l1)
print res4.status_code, res4.content

dict1 = {'course': 'BDPY', 'description': 'python with Spark, pycharm'}
url5 = url + 'data5_dict.json'
res5 = requests.put(url5, json=dict1)
print res5.status_code, res5.content

url6 = url + '/some/path/to/any/dir/.json'
res6 = requests.put(url6, json='Hi, I am here!')
print res6.status_code, res6.content
