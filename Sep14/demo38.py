# encoding=utf8
import Tkinter
import tkFont


def func1():
    label.config(text='press')


def func2():
    label.config(text='lease')


def func3():
    label.config(text='enter')


# set a new window
top = Tkinter.Tk()
myFont = tkFont.Font(family='Arial', size=30)
label = Tkinter.Label(top, text='Which Action?', font=myFont)
btn1 = Tkinter.Button(top, text='click', command=func1)

btn1.bind('<Leave>', func2)
btn1.bind('<Enter>', func3)

# set all items
label.pack()
btn1.pack()

# open new window
top.mainloop()
