# encoding=utf8
import Tkinter
import tkFont


def leftSingle(ev):
    label.coonfig(text='left button click', fg='#F00')


# set a new window
top = Tkinter.Tk()
myFont = tkFont.Font(family='Source Code Pro', size=30)
label = Tkinter.Label(top, text='more event', font=myFont)
btn1 = Tkinter.Button(top, text='click')

btn1.bind('<Button-1>', leftSingle)

# set all items
label.pack()
btn1.pack()

# open new window
top.mainloop()
