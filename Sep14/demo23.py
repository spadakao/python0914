# encoding=utf-8
import os
import sys

print 'current dir = %s' % (os.getcwd())

# mkdir can only execute once
# 創建不能創名字重複的
# os.mkdir('foo')
# os.mkdir(u'中文')

# remove 不能移除不存在的
# os.rmdir(u'中文')

# chdir 不能移到不存在的
# os.chdir('foo')
# os.chdir(u'中文')
print 'current dir = %s' % (os.getcwd())

# 印出 執行時賦予的參數
# pycharm 內要加入參數 => 右上角 demo23 dropdown button => Edit configurations... => Script parameters
for item in sys.argv:
    print item

# 執行的 py 版本
print sys.executable