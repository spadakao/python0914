# encoding=utf-8
import itertools
import json

result = itertools.chain('abc', '123', u'甲乙丙')
for item in result:
    print item

print '-----'
result2 = itertools.permutations('abcd', 2)
l1 = [item for item in result2]
print l1, len(l1)

print '-----'
result3 = itertools.combinations('abcde', 3)
l2 = [item for item in result3]
print l2, len(l2)

# python dict 轉 json
# dumps() => python dict or list 轉為 json string
# loads() => json string 轉為 python dict or list
print '-----'
data1 = {'name': 'Mark', 'age': 42, 'weight': 73.5}
str1 = json.dumps(data1)
print str1
print json.loads(str1)['age'], json.loads(str1)['weight']
print type(json.loads(str1))

print '-----'
data2 = ['Sun', 'Mon', u'週二']
str2 = json.dumps(data2)
print str2
print json.loads(str2)[0], json.loads(str2)[1], json.loads(str2)[2]

# shell utility
import shutil

# shutil.rmtree('bar')
# shutil.copytree('foo', 'bar')
