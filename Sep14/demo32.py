# encoding=utf-8
import requests

# requests.get() 應用，讀取資料
url = 'https://pylesson-23f40.firebaseio.com/'

url1 = url + 'data1_string.json'
res1 = requests.get(url1)
print res1.status_code, res1.content, type(res1.content)

url2 = url + 'data2_chinese.json'
res2 = requests.get(url2)
print res2.status_code, res2.content, type(res2.content)

url4 = url + 'data4_array.json'
res4 = requests.get(url4)
print res4.status_code, type(res4.content), type(res4.json())
for item in res4.json():
    print item

url5 = url + 'data5_dict.json'
res5 = requests.get(url5)
print res5.status_code, type(res5.content), type(res5.json())
for k, v in res5.json().items():
    print k + '/' + str(v)

url6 = url + 'bid1.json'
res6 = requests.get(url6)
print type(res6.json())
for v in res6.json().values():
    for k1, v1 in v.items():
        print 'record: %s, %s' % (k1, str(v1))
